FROM rocker/shiny
LABEL maintainer="Veit Schwaemmle <veits@bmb.sdu.dk>"
LABEL description="Docker image of CoExpresso implementation on top of shiny-server. The number of to-be-installed R packages requires patience when building this image. Needs to be run mapping the folder with the random scores and matrices to /srv/shiny-server/PCS"

RUN apt-get update && apt-get install -y libssl-dev liblzma-dev libbz2-dev libicu-dev libxml2-dev libglpk-dev && apt-get clean 


RUN R -e "install.packages('BiocManager'); \
  update.packages(ask=F); \
  BiocManager::install(ask=F); \
  BiocManager::install(c('dplyr','plotly'),ask=F)"
RUN R -e "library(BiocManager); BiocManager::install(c('matrixStats','igraph','parallel','lattice','plyr','gplots','data.table','clusterProfiler','org.Hs.eg.db','STRINGdb','jsonlite','shinydashboard','shinyBS','DT','limma'\
),ask=F)"
RUN R -e "library(BiocManager); BiocManager::install(c('biomaRt'),ask=F)"
RUN rm -rf /srv/shiny-server
RUN mkdir /srv/shiny-server
#RUN mkdir /srv/shiny-server/PCS
#RUN mkdir /srv/shiny-server/PCS/www
#COPY PCS/* /srv/shiny-server/PCS/
#COPY www/* /srv/shiny-server/PCS/
COPY *R  /srv/shiny-server/
RUN mkdir /srv/shiny-server/www
COPY www/* /srv/shiny-server/www/

