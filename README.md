## CoExpresso: Protein co-regulation in the human proteome

For more details see the manuscript: https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2573-8

### Usage
Access the web service via computproteomics.bmb.sdu.dk/Apps/CoExpresso

You can investigate any group of human proteins (number < 40) for their co-regulation in human cells. The app provides statistical evidence for their co-regulation, shows different visualizations and compares the restults to protein-protein interations in STRINGdb.

### Installation of own shiny server
You need to install the necessary R libraries and also place the files for the randomized protein groups in the folder _PCS_. The files are available upon request (7GB).

Used R libraries: shiny, shinydashboard, STRINGdb, igraph, lattice, plyr, biomaRt, gplots, data.table, ,clusterProfiler, org.Hs.eg.db


### Running R script for generation of randomized protein groups and evaluation of protein complexes (from the CORUM database)

Execute https://bitbucket.org/veitveit/coexpresso/src/master/GraphOfComplex.R after modification to include correct paths and installation of all libraries.

Be aware that you need at least 32 GB of memory and a lot of patience to run this script.



