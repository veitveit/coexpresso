README for CoExpressoRandomization for the CoExpresso Tool (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6327379/)

Created by: David Sandholzer, 24.05.2024

#######################################################################################################################################################

How does the prot-matrix have to look like?

The file must be a csv-file where the elements are separated by commas and the values must be double values (no N.A. allowed). 
Headers must be removed (Protein names and Cell types).

#######################################################################################################################################################

How to run the tool on UCloud:

0. start a Coder++ session on UCloud
Ideally choose a 64 core-machine. The radomization for each protein group size will be computed parallel so for group sizes from 2 to 50, 48 threads
are going to run at once.

1. change to the right folder
cd CoExpressoRandomization/

2. compile the program with g++ (all .cpp files must be included, any output file name can be used)
g++ main.cpp pcs_master.cpp pcs_worker.cpp -o run

3. run the compiled file
./run

#######################################################################################################################################################

How to change the settings (including prot-matrix) for the tool:

1. settings can be found in the config.h file

2. read descriptions of each setting (see comments in code) and edit if needed
Most important settings:
- INPUT_FILE_NAME: prot-matrix filename
- TRIALS: Number of sampled protein groups per combination of n_prot und n_tissues (as descr. in paper), exmample values:
	=> ProteoimicsDB: 20000
	=> CPTAC: 500 (lower value because of the huge amount of cell types!)
- MIN_PROTS & MAX_PROTS: allowed protein group sizes (default 2-50)
	=> note: having around or more than 64 (max - min) will slow down the randomization tool because not all can be run at the same time
- MAX_SCORES_PER_FILE: limit the amount of lines per result file

3. compile and run the program

#######################################################################################################################################################

How to download the up-to-date prot-matrix from proteomicsDB?

0. start a Coder Python session on UCloud
machine type does not really matter, everything is singlethreaded

1. change to the right folder
cd CoExpressoRandomization/

2. install packages:
pip install pandas
pip install requests

3. the script should be done in 20-30 minutes

4. result file can be used for the CoExpressoRandomization tool.
use the prot_matrix_api.csv file (without headers)

#######################################################################################################################################################
