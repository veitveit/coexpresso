#include "pcs_master.h"

int main()
{
	try
	{
		pcs_master pcs(pcs_config::INPUT_MATRIX_FILE);
		pcs.compute();
	}
	catch (std::exception const& ex)
	{
		std::cerr << "Unexpected error during runtime: " << ex.what() << std::endl;
	}

	return 0;
}
