#ifndef PCS_MASTER
#define PCS_MASTER

#include "pcs_worker.h"

#include <stdexcept>

class pcs_master
{
	size_t total_cell_types;

	std::vector<std::vector<double>*> original_matrix;
	std::vector<std::bitset<pcs_config::MAX_NUM_BITS>> prot_matrix;
	std::vector<std::vector<int>> shuffle_vectors;

public:
	pcs_master() = delete;
	pcs_master(std::string const& file_name);
	pcs_master(pcs_master const& src) = delete;
	~pcs_master();

	void compute();

	void get_num_prot_foreach_min_tissues(std::string const& file_name);

private:
	void initialize();

	void create_shuffle_vector();

	size_t read_matrix(std::string const& file_name);
	void build_bit_matrix();

	static void write_score_matrix_row(std::vector<size_t> const& row, std::ofstream& ofs);

	void clear_original_matrix();
	void clear_shuffle_vectors();
};

#endif // !PCS_MASTER
