#ifndef PCS_CONFIG_H
#define PCS_CONFIG_H

namespace pcs_config
{
	// name of the input prot-matrix (from proteomicsdb, CPTAC, ...)
	static const char* INPUT_MATRIX_FILE = "prot_matrix_api.csv";

	// max number of different cell types (set to 6000, because CPTAC has 5k something)
	static const int MAX_NUM_BITS = 6000;

	// looping over all cell types can be reduced by incrementing by higher amounts than one
	static const int NTISSUE_INCR = 1;

	// looping over all cell types start value (first run will include all proteins that are expressed in at least MIN_NTISSUES tissues)
	static const int MIN_NTISSUES = 2;

	// minimum amount of available proteins where the sampling is done (for higher ntissues there will be less and less proteins that are expressed in that many cell types)
	static const int MIN_AVAILABLE_PROTEINS = 1000;

	// max protein group size
	static const int MAX_NPROTS = 50;

	// min protein group size
	static const int MIN_NPROTS = 2;

	// amount of samples for each combination of nprot and ntissues (5000 in the CoExpresso paper)
	static const int TRIALS = 20000;

	// min amount of common tissues for a protein group to be accepted (and to calculate the correlations)
	static const int MIN_TISSUES = 5;

	// if for any nprot, there were less duplicates than MIN_DUPLICATES, every greater nprot will not search for duplicates in the result files
	static const int MIN_DUPLICATES = 1;

	// limits the amount of scores of an output file; when reached, every sampled prot group with the respective nprot and n_common_tissues will be rejected
	static const int MAX_SCORES_PER_FILE = 100000;
}

#endif // !PCS_CONFIG_H
