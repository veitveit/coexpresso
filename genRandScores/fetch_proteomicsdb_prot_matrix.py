import requests
import pandas as pd

url_tiss = "https://www.proteomicsdb.org/proteomicsdb/logic/api/tissuelist.xsodata/CA_AVAILABLEBIOLOGICALSOURCES_API?$select=TISSUE_ID,TISSUE_NAME,TISSUE_GROUP_NAME,TISSUE_CATEGORY,SCOPE_ID,SCOPE_NAME,QUANTIFICATION_METHOD_ID,QUANTIFICATION_METHOD_NAME,MS_LEVEL&$format=json"
response_tiss = requests.get(url_tiss)

if response_tiss.status_code != 200:
    print("cannot load cell types")
    exit(1)

data = response_tiss.json()

tissue_ids = {entry["TISSUE_ID"] for entry in data["d"]["results"] if str(entry["TISSUE_ID"]).startswith("BTO")}
#print(tissue_ids)

#tissue_beginnings = {entry["TISSUE_ID"][:3] for entry in data["d"]["results"] if str(entry["TISSUE_ID"])}
#print(tissue_beginnings)       # should print BTO, PDB and PO

df = pd.DataFrame(columns=list(tissue_ids))
df_cnt = pd.DataFrame(columns=list(tissue_ids))

nm = 0
for tiss_id in tissue_ids:
    url_prt = f"https://www.proteomicsdb.org/proteomicsdb/logic/api/proteinspertissue.xsodata/InputParams(TISSUE_ID='{tiss_id}',CALCULATION_METHOD=0,SWISSPROT_ONLY=1,NO_ISOFORM=1,TAXCODE=9606)/Results?$select=ENTRY_NAME,UNIQUE_IDENTIFIER,DATABASE,PROTEIN_DESCRIPTION,PEPTIDES,TISSUE_ID,SAMPLE_NAME,SAMPLE_DESCRIPTION,UNNORMALIZED_EXPRESSION,NORMALIZED_EXPRESSION&$format=json"

    response = requests.get(url_prt)

    if response.status_code == 200:
        data = response.json()

        results = data['d']['results']

        cnt = 0
        for result in results:
            cnt += 1

            unique_identifier = result["UNIQUE_IDENTIFIER"]

            if unique_identifier not in df.index:
                df.loc[unique_identifier] = 0.0
                df_cnt.loc[unique_identifier] = 0

            df.loc[unique_identifier, tiss_id] += float(result["NORMALIZED_EXPRESSION"])
            df_cnt.loc[unique_identifier, tiss_id] += 1

        print("tiss:", tiss_id, "cnt:", cnt)
        if cnt > 0:
            nm += 1
        else:
            df = df.drop(tiss_id, axis=1)
            df_cnt = df_cnt.drop(tiss_id, axis=1)

        #if nm > 3:
            #break

    else:
        print("Failed to fetch data:", response.status_code)

df = df.div(df_cnt)

df.fillna(0, inplace=True)

df.to_csv('prot_matrix_api.csv', header=False, index=False)
df.to_csv('prot_matrix_api_with_headers.csv', header=True, index=True)
