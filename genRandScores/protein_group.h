#ifndef PROTEIN_GROUP_T
#define PROTEIN_GROUP_T

#include <vector>

struct protein_group_t
{
	protein_group_t() = delete;
	protein_group_t(protein_group_t const& src) = delete;
	~protein_group_t() {
		delete this->i_proteins;
		this->i_proteins = nullptr;

		delete this->intensities;
		this->intensities = nullptr;
	}

	protein_group_t(std::vector<int>* i_proteins, int const num_cell_types, std::vector<size_t>* intensity_indexes)
	{
		this->i_proteins = i_proteins;
		this->num_cell_types = num_cell_types;
		this->intensities = intensity_indexes;
	}

	std::vector<int>* i_proteins;
	int num_cell_types;
	std::vector<size_t>* intensities;
};

#endif // !PROTEIN_GROUP_T
