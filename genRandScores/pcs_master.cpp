#include "pcs_master.h"

#include <filesystem>
#include <thread>

using std::vector;
using std::string;
using std::cout;
using namespace pcs_config;

pcs_master::pcs_master(string const& file_name)
{
	this->total_cell_types = this->read_matrix(file_name);
	std::filesystem::create_directories(pcs_worker::get_out_folder_path());
}

pcs_master::~pcs_master()
{
	this->clear_original_matrix();
	this->clear_shuffle_vectors();
}

void pcs_master::compute()
{
	// start measure
	auto start(std::chrono::high_resolution_clock::now());

	this->initialize();

	vector<pcs_worker*> nprot_workers(MAX_NPROTS + 1, nullptr);
	vector<std::thread> nprot_threads(MAX_NPROTS + 1);

	for (size_t n_prot{ MIN_NPROTS }; n_prot <= MAX_NPROTS; ++n_prot)
	{
		nprot_workers[n_prot] = new pcs_worker(&this->original_matrix, &this->prot_matrix, this->shuffle_vectors, this->total_cell_types);
		nprot_threads[n_prot] = std::thread(&pcs_worker::sample_and_corr, nprot_workers[n_prot], n_prot);
	}

	std::ofstream ofs("sample.csv");

	// STEP 3.2: remove duplicates
	size_t cnt_dupl(MIN_DUPLICATES);
	for (size_t n_prot{ MIN_NPROTS }; n_prot <= MAX_NPROTS; ++n_prot)
	{
		nprot_threads[n_prot].join();
		
		if (cnt_dupl >= MIN_DUPLICATES)
		{
			cnt_dupl = nprot_workers[n_prot]->remove_duplicates(n_prot);
			cout << cnt_dupl << " duplicates removed for nprot: " << n_prot << std::endl;
		}

		if (nprot_workers[n_prot] != nullptr)
		{
			this->write_score_matrix_row(nprot_workers[n_prot]->get_count_matrix_row(), ofs);
			delete nprot_workers[n_prot];
			nprot_workers[n_prot] = nullptr;
		}
	}

	ofs.close();

	auto end(std::chrono::high_resolution_clock::now());

	std::chrono::duration<double> duration(end - start);

	cout << "PCS done in " << duration.count() << " seconds" << std::endl;
}

void pcs_master::get_num_prot_foreach_min_tissues(std::string const& file_name)
{
	// for Debug purposes only (to check the prot matrix): shows the amount of proteins that have n tissue expressions
	// example output:
	// n_tiss 160: 1112
	// n_tiss 161: 1038
	// n_tiss 162: 979
	this->initialize();

	std::ofstream ofs(file_name);

	for (size_t n_tissue{ MIN_NTISSUES }; n_tissue < this->shuffle_vectors.size(); n_tissue += NTISSUE_INCR)
	{
		ofs << "n_tiss " << n_tissue << ": " << this->shuffle_vectors[n_tissue].size() << std::endl;
	}

	ofs.close();
}

void pcs_master::initialize()
{
	if (this->original_matrix.size() == 0)
	{
		throw std::runtime_error("Protein matrix is empty!");
	}

	if (this->total_cell_types <= MIN_NTISSUES)
	{
		throw std::runtime_error("Too few cell types: " + std::to_string(this->total_cell_types));
	}

	// create bit matrix from input (file matrix)
	this->build_bit_matrix();

	// prepare randomization vectors for each n_tissue beforehand
	this->create_shuffle_vector();
}

void pcs_master::create_shuffle_vector()
{
	this->clear_shuffle_vectors();

	this->shuffle_vectors = vector<vector<int>>(this->total_cell_types + 1);

	// prepare tissue based shuffle vectors (for sampling)
	for (size_t n_tissue{ MIN_NTISSUES }; n_tissue <= this->total_cell_types; n_tissue += NTISSUE_INCR)
	{
		// STEP 1: only allow proteins with at least n_tissues 
		for (size_t prot{ 0 }; prot < this->prot_matrix.size(); ++prot)
		{
			if (this->prot_matrix[prot].count() >= n_tissue)
			{
				this->shuffle_vectors[n_tissue].push_back(prot);
			}
		}
	}
}

size_t pcs_master::read_matrix(string const& file_name)
{
	this->clear_original_matrix();

	auto start = std::chrono::high_resolution_clock::now();

	this->original_matrix = vector<vector<double>*>();

	size_t num_tissues(0);

	std::ifstream ifs(file_name);
	while (ifs.good())
	{
		string line;
		std::getline(ifs, line);

		// read all lines; all non-empty lines must contain the same amount of cells separated by whitespaces or tabs
		if (!line.empty())
		{
			vector<double>* vec_c_type_n(new vector<double>);
			vec_c_type_n->reserve(num_tissues);

			std::stringstream str(line);
			while (str.good())
			{
				string val;
				std::getline(str, val, ',');
				if (!val.empty())
				{
					vec_c_type_n->push_back(std::stod(val));
				}
			}

			size_t vec_size(vec_c_type_n->size());
			if (num_tissues == 0)
			{
				if (vec_size > 0)
				{
					num_tissues = vec_size;
				}
				else
				{
					throw std::runtime_error("Error current matrix row does not contain any valid elements!");
				}
			}
			else
			{
				if (vec_c_type_n->size() != num_tissues)
				{
					throw std::runtime_error("Error matrix rows do not have the same sizes!");
				}
			}

			this->original_matrix.push_back(vec_c_type_n);
		}
	}

	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> duration = end - start;

	cout << "matrix read in " << duration.count() << " seconds" << std::endl;

	return num_tissues;
}

void pcs_master::build_bit_matrix()
{
	this->prot_matrix = vector<std::bitset<MAX_NUM_BITS>>(this->original_matrix.size());

	for (size_t i{ 0 }; i < this->original_matrix.size(); ++i)
	{
		for (size_t j{ 0 }; j < this->original_matrix[i]->size(); ++j)
		{
			if ((*this->original_matrix[i])[j] != 0)
			{
				this->prot_matrix[i].set(j);
			}
		}
	}
}

void pcs_master::write_score_matrix_row(std::vector<size_t> const& row, std::ofstream& ofs)
{
	bool first(true);
	for (size_t i{ 0 }; i < row.size(); ++i)
	{
		if (!first)
		{
			ofs << "; ";
		}

		first = false;
		ofs << row[i];
	}

	ofs << std::endl;
}

void pcs_master::clear_original_matrix()
{
	for (size_t i{ 0 }; i < this->original_matrix.size(); ++i)
	{
		delete this->original_matrix[i];
		this->original_matrix[i] = nullptr;
	}
}

void pcs_master::clear_shuffle_vectors()
{
	for (size_t i_tiss{ 0 }; i_tiss < this->shuffle_vectors.size(); ++i_tiss)
	{
		this->shuffle_vectors[i_tiss].clear();
	}
}
