#ifndef PCS_WORKER_H
#define PCS_WORKER_H

#include "pcs_config.h"

#include <vector>
#include <string>
#include <bitset>
#include <random>
#include <fstream>
#include <iostream>
#include <sstream>
#include <chrono>

class pcs_worker
{
	std::random_device rd;
	std::mt19937 gen;

	std::vector<std::vector<double>*>* original_matrix;
	std::vector<std::bitset<pcs_config::MAX_NUM_BITS>>* prot_matrix;
	std::vector<std::vector<int>> shuffle_vectors;

	std::vector<size_t> count_matrix_row;

	size_t num_tissues;

public:
	pcs_worker() = delete;
	pcs_worker(std::vector<std::vector<double>*>* original_matrix,
		std::vector<std::bitset<pcs_config::MAX_NUM_BITS>>* prot_matrix,
		std::vector<std::vector<int>> const& shuffle_vectors,
		size_t const num_tissues);
	pcs_worker(pcs_worker const& src) = delete;
	~pcs_worker() = default;

	void sample_and_corr(size_t const n_prot);

	size_t remove_duplicates(size_t const n_prot);

	std::vector<size_t> const& get_count_matrix_row() const;

	static std::string const& get_out_folder_path();
	
	static std::string const get_output_file_name(size_t const n_tissues, size_t const n_prot);

private:

	void part_shuffle_vector(std::vector<int>& shuf_vect, int const n_prot);

	void compute_correlations(std::vector<int> const& prot_grp, std::vector<size_t> const& intensities, size_t const n_prot, std::ofstream* ofs) const;

	static double get_mean(std::vector<double> const& vect);
	static double get_median(std::vector<double>& vect);
	static double get_stddev(std::vector<double> const& vect, double const mean);

	static bool compare_by_mean(std::tuple<double, double, double> const& lhs, std::tuple<double, double, double> const& rhs);
};

#endif // !PCS_WORKER_H
