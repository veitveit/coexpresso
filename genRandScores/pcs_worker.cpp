#include "pcs_worker.h"

#include <algorithm>
#include <numeric>
#include <tuple>
#include <sstream>
#include <thread>

using std::vector;
using std::string;
using std::bitset;
using namespace pcs_config;

pcs_worker::pcs_worker(vector<vector<double>*>* original_matrix, vector<bitset<MAX_NUM_BITS>>* prot_matrix, vector<vector<int>> const& shuffle_vectors, size_t const num_tissues)
{
	this->gen = std::mt19937(this->rd());
	this->original_matrix = original_matrix;
	this->prot_matrix = prot_matrix;
	this->shuffle_vectors = vector<vector<int>>(shuffle_vectors);
	this->count_matrix_row = vector<size_t>(num_tissues + 1);
	this->num_tissues = num_tissues;
}

void pcs_worker::sample_and_corr(size_t const n_prot)
{
	size_t cnt(0);
	vector<std::ofstream*> ofss(this->num_tissues + 1, nullptr);
	for (size_t n_tissue{ MIN_NTISSUES }; n_tissue <= this->num_tissues; n_tissue += NTISSUE_INCR)
	{
		auto& shuff_vect = this->shuffle_vectors[n_tissue];

		// only sample if there are at least MIN_AVAILABLE_PROTEINS proteins that are present in at least n_tissue cell types.
		if (shuff_vect.size() >= MIN_AVAILABLE_PROTEINS)
		{
			// STEP 2: TRIALS
			for (size_t trial{ 0 }; trial < TRIALS; ++trial)
			{
				// random protein group sampling
				this->part_shuffle_vector(shuff_vect, n_prot);

				// bitwise AND to get common cell types for all proteins
				bitset<MAX_NUM_BITS> result_set = (*this->prot_matrix)[shuff_vect[0]];
				for (size_t i{ 1 }; i < n_prot; ++i)
				{
					result_set &= (*this->prot_matrix)[shuff_vect[i]];
				}

				size_t common_ct(result_set.count());

				// STEP 3.1: min 5 common cell types; plus check if max lines per file has not been reached
				if (common_ct >= MIN_TISSUES && this->count_matrix_row[common_ct] <= MAX_SCORES_PER_FILE)
				{
					// sort vector for removing duplicates later (so the lines in the file come in the same order)
					std::sort(shuff_vect.begin(), shuff_vect.begin() + n_prot);

					// get (indexof) common cell types
					vector<size_t> intensity_indexes;
					intensity_indexes.reserve(common_ct);
					for (size_t i{ 0 }; i < result_set.size(); ++i)
					{
						if (result_set.test(i))
						{
							intensity_indexes.push_back(i);
						}
					}

					this->count_matrix_row[common_ct] += n_prot;

					if (ofss[common_ct] == nullptr)
					{
						ofss[common_ct] = new std::ofstream(get_output_file_name(common_ct, n_prot));
					}

					this->compute_correlations(shuff_vect, intensity_indexes, n_prot, ofss[common_ct]);

					cnt++;
				}
			}
		}
	}

	for (size_t i{ 0 }; i < ofss.size(); ++i)
	{
		if (ofss[i] != nullptr)
		{
			ofss[i]->close();
			delete ofss[i];
			ofss[i] = nullptr;
		}
	}

	std::cout << "sampling and correlations finished for n_prot = " << n_prot << " with " << cnt << " protein groups!" << std::endl;
}

size_t pcs_worker::remove_duplicates(size_t const n_prot)
{
	// read correlations from file to remove duplicates
	size_t cnt_dupl(0);
	vector<string*> tmp_corrs;
	for (int i{ 0 }; i < this->count_matrix_row.size(); ++i)
	{
		if (this->count_matrix_row[i] > 0)
		{
			tmp_corrs.reserve(this->count_matrix_row[i] / n_prot);

			// read correlations from file
			std::ifstream ifs(get_output_file_name(i, n_prot));
			while (ifs.good())
			{
				string tmp_grp_corr = "";
				for (size_t j{ 0 }; j < n_prot; ++j)
				{
					string line;
					std::getline(ifs, line);
					tmp_grp_corr += j == 0 ? line : (":" + line);
				}

				tmp_corrs.push_back(new string(tmp_grp_corr));
			}

			ifs.close();

			tmp_corrs.erase(std::remove_if(tmp_corrs.begin(), tmp_corrs.end(), [](string const* const corrs) {
				return corrs->find(';') == string::npos;
				}), tmp_corrs.end());

			// sort the vector of pointers
			std::sort(tmp_corrs.begin(), tmp_corrs.end(), [](std::string* lhs, std::string* rhs) { return *lhs < *rhs; });

			// remove adjacent duplicates
			auto it = std::unique(tmp_corrs.begin(), tmp_corrs.end(), [](std::string* lhs, std::string* rhs) { return *lhs == *rhs; });

			// get the number of removed duplicates
			size_t removed(tmp_corrs.end() - it);
			cnt_dupl += removed;

			// erase the removed duplicates
			tmp_corrs.erase(it, tmp_corrs.end());

			// if any element was removed => file must be updated and count matrix row!
			if (removed > 0)
			{
				this->count_matrix_row[i] -= (removed * n_prot);

				std::ofstream ofs(get_output_file_name(i, n_prot));
				for (size_t j{ 0 }; j < tmp_corrs.size(); ++j)
				{
					std::stringstream str(*tmp_corrs[j]);
					while (str.good())
					{
						string yoo;
						std::getline(str, yoo, ':');

						ofs << yoo << std::endl;
					}
				}

				ofs.close();
			}

			for (size_t j{ 0 }; j < tmp_corrs.size(); ++j)
			{
				delete tmp_corrs[j];
			}

			tmp_corrs.clear();
		}
	}

	return cnt_dupl;
}

std::vector<size_t> const& pcs_worker::get_count_matrix_row() const
{
	return this->count_matrix_row;
}

void pcs_worker::part_shuffle_vector(vector<int>& shuf_vect, int const n_prot)
{
	// Fisher-Yates like shuffle
	for (size_t i{ 0 }; i < n_prot; ++i)
	{
		// new dist for each iteration => may take slightly more time but is cleaner :)
		std::uniform_int_distribution<int> dist(i, shuf_vect.size() - 1);
		int j = dist(this->gen);
		std::swap(shuf_vect[i], shuf_vect[j]);
	}
}

void pcs_worker::compute_correlations(vector<int> const& prot_grp, vector<size_t> const& intensities, size_t const n_prot, std::ofstream* ofs) const
{
	size_t common_tissues(intensities.size());
	vector<vector<double>> pairwise_correlations(n_prot);			// pairwise matrix to save all combinations
	vector<double> mean_correlations(n_prot);

	// calculate correlationS for the protein group... 

	vector<double> mean_intensities(common_tissues, 0);
	for (size_t i_protein{ 0 }; i_protein < n_prot; ++i_protein)
	{
		auto& curr_intensity_row((*(*this->original_matrix)[prot_grp[i_protein]]));
		for (size_t i_intensity{ 0 }; i_intensity < common_tissues; ++i_intensity)
		{
			mean_intensities[i_intensity] += curr_intensity_row[intensities[i_intensity]];
		}
	}

	for (size_t i_intensity{ 0 }; i_intensity < common_tissues; ++i_intensity)
	{
		mean_intensities[i_intensity] /= n_prot;
	}

	double mean_mean(get_mean(mean_intensities));
	double mean_stddev(get_stddev(mean_intensities, mean_mean));

	// calc means and stddevs for the intensities for each protein
	vector<double> means(n_prot);
	vector<double> std_devs(n_prot);

	// foreach protein in group: calculate mean and std dev for all tissue abundances that are present in all proteins from the protein group
	for (size_t i_protein{ 0 }; i_protein < n_prot; ++i_protein)
	{
		pairwise_correlations[i_protein].clear();		// clear pairwise correlations from previous protein group!

		auto& intensity_row(*(*this->original_matrix)[prot_grp[i_protein]]);

		double sum_i(0), sum_j(0);
		for (size_t i_common_cell_type{ 0 }; i_common_cell_type < common_tissues; ++i_common_cell_type)
		{
			sum_i += intensity_row[intensities[i_common_cell_type]];
		}

		double mean_i(sum_i / common_tissues);

		double sum_sq_diff_i(0.0), sum_sq_diff_j(0.0);
		for (size_t i_common_cell_type{ 0 }; i_common_cell_type < common_tissues; ++i_common_cell_type)
		{
			sum_sq_diff_i += (intensity_row[intensities[i_common_cell_type]] - mean_i)
				* (intensity_row[intensities[i_common_cell_type]] - mean_i);
		}

		double std_dev_i(sqrt(sum_sq_diff_i / common_tissues));

		// store in vectors mean and std dev for protein on i_protein index i
		means[i_protein] = mean_i;
		std_devs[i_protein] = std_dev_i;
	}

	// calculate correlations for each combination of 2 proteins inside a group and for each protein to the group mean intensities
	for (size_t i_protein{ 0 }; i_protein < n_prot; ++i_protein)
	{
		// pairwise
		auto& intensity_row_i(*(*this->original_matrix)[prot_grp[i_protein]]);
		for (size_t j_protein{ i_protein + 1 }; j_protein < n_prot; ++j_protein)
		{
			auto& intensity_row_j(*(*this->original_matrix)[prot_grp[j_protein]]);
			double curr_corr_sum(0.0);
			for (size_t i_common_cell_type{ 0 }; i_common_cell_type < common_tissues; ++i_common_cell_type)
			{
				size_t i_tissue(intensities[i_common_cell_type]);

				curr_corr_sum += ((intensity_row_i[i_tissue] - means[i_protein]) / std_devs[i_protein])
					* ((intensity_row_j[i_tissue] - means[j_protein]) / std_devs[j_protein]);
			}

			double corr(curr_corr_sum / common_tissues);
			pairwise_correlations[i_protein].push_back(corr);		// add correlation of the group
			pairwise_correlations[j_protein].push_back(corr);		// mirror!
		}

		// mean
		double curr_corr_sum(0.0);
		for (size_t i_common_cell_type{ 0 }; i_common_cell_type < common_tissues; ++i_common_cell_type)
		{
			size_t i_tissue(intensities[i_common_cell_type]);

			curr_corr_sum += ((intensity_row_i[i_tissue] - means[i_protein]) / std_devs[i_protein])
				* ((mean_intensities[i_common_cell_type] - mean_mean) / mean_stddev);
		}

		mean_correlations[i_protein] = (curr_corr_sum / common_tissues);
	}

	// ONE LINE FOR EACH PROTEIN WITH MEAN PAIRWISE, MEDIAN PAIRWISE and CORRELATION TO MEAN
	for (size_t i_protein{ 0 }; i_protein < n_prot; ++i_protein)
	{
		double mean(get_mean(pairwise_correlations[i_protein]));
		double median(get_median(pairwise_correlations[i_protein]));
		*ofs << mean << ";" << median << ";" << mean_correlations[i_protein] << std::endl;
	}
}

double pcs_worker::get_mean(vector<double> const& vect)
{
	if (vect.empty())
	{
		return 0.0;
	}

	return std::accumulate(vect.begin(), vect.end(), 0.0) / vect.size();
}

double pcs_worker::get_median(vector<double>& vect)
{
	if (vect.empty())
	{
		return 0.0;
	}

	std::sort(vect.begin(), vect.end());
	size_t n(vect.size());
	return n % 2 == 0 ? (vect[n / 2 - 1] + vect[n / 2]) / 2.0 : vect[n / 2];
}

double pcs_worker::get_stddev(vector<double> const& vect, double const mean)
{
	double sum_squared_diff = 0.0;
	for (double val : vect)
	{
		sum_squared_diff += (val - mean) * (val - mean);
	}

	return sqrt(sum_squared_diff / vect.size());
}

bool pcs_worker::compare_by_mean(std::tuple<double, double, double> const& lhs, std::tuple<double, double, double> const& rhs)
{
	return std::get<0>(lhs) < std::get<0>(rhs);
}

string const& pcs_worker::get_out_folder_path()
{
	static const string fp{ "./output_files" };
	return fp;
}

string const pcs_worker::get_output_file_name(size_t const n_tissues, size_t const n_prot)
{
	std::ostringstream oss;
	oss << get_out_folder_path() << "/prg_t" << n_tissues << "_p" << n_prot << ".csv";
	return oss.str();
}
